import React from 'react'

function Profile() {
	return (
		<div className='content'>
			<div>
				<img
					alt='panorama'
					src='https://images.unsplash.com/photo-1513735539099-cf6e5d559d82?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
				></img>
			</div>
			<div>avia + description</div>
			<div>
				my posts
				<div>New post</div>
				<div>
					<div>Post 1</div>
					<div>Post 2</div>
					<div>Post 3</div>
				</div>
			</div>
		</div>
	)
}
export default Profile
